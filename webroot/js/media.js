$(document).ready(function() {
    $('#thumbnail').on('change', function() {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
    $('#type').on('change', function() {
        switch ($(this).val()) {
            case '1':
                $('label[for="media"]').html('Choose Video Media');
                $('#media[type="file"]').attr('accept', 'video/*');
                break;
            case '2':
                $('label[for="media"]').html('Choose Audio Media');
                $('#media[type="file"]').attr('accept', 'audio/*');
                break;
            default:
                $('label[for="media"]').html('Choose Image Media');
                $('#media[type="file"]').attr('accept', 'image/*');
        }
    });
    $('#type').trigger('change');
    $('#media').on('submit', function(ev) {
        var mediaData = $('#medi[type="file"]a').val();
        var linkData = $('#link').val();
        if ((mediaData.length === 0 && linkData.length === 0) || mediaData.length !== 0 && linkData.length !== 0) {
            ev.preventDefault();
            alert('Please upload media or enter media link.');
            return false;
        }
    })
});