$(document).ready(function () {
    $('[id$=image]').on('change', function () {
        if (this.files && this.files[0]) {
            $('#thumbnail-' + $(this).attr('id')).attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-' + $(this).attr('id')).attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
});