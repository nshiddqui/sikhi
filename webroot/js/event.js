$(document).ready(function () {
    dateSet();
    $('#image').on('change', function () {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });

    $('#file').on('change', function () {
        if (this.files && this.files[0]) {
            $('#file-path').html(this.files[0]['name']);
        } else {
            $('#file-path').html('Choose an ICS file or drag it here.');
        }
    });
    $('#start-date').on('change', function () {
        $('#end-date').datepicker('setStartDate', $(this).val());
    });
    $('#end-date').on('change', function () {
        $('#start-date').datepicker('setEndDate', $(this).val());
    });
    $('#is-event-0, #is-event-1').on('click', dateSet);
});
function dateSet() {
    if ($('#is-event-0').is(':checked')) {
        $('#end-date').parent('div').show();
        $('#end-date').prop('required', true);
        $('label[for="start-date"]').html('Start Date');
    } else {
        $('#end-date').parent('div').hide();
        $('#end-date').prop('required', false);
        $('label[for="start-date"]').html('Date');
    }
}