$(document).ready(function() {
    if (typeof pdf_no === 'undefined') {
        var pdf_no = 2;
    }
    $('#div-' + $('[id^=type]:checked').attr('id')).show();
    if ($('#type-0:checked').length > 0) {
        $('#plain-english, #plain-punjabi').prop('required', true);
    }
    $('[id^=type]').on('click', function() {
        var $id = $(this).attr('id');
        if ($id === 'type-0') {
            $('#plain-english, #plain-punjabi').prop('required', true);
        } else {
            $('#plain-english, #plain-punjabi').prop('required', false);
        }
        $('.type-div').hide();
        $('#div-' + $id).show();
    });
    $('[id^=audio-plain], [id^=pdf-], [id^=audio-], [id^=video-]').on('change', function(event) {
        var $id = $(this).attr('id');
        if (this.files && this.files[0]) {
            $('#name-' + $id).html(this.files[0]['name']);
        } else {
            $('#name-' + $id).html('');
        }
    });
    $('#image').on('change', function() {
        if (this.files && this.files[0]) {
            $('#thumbnail-image').attr('src', URL.createObjectURL(this.files[0]));
        } else {
            $('#thumbnail-image').attr('src', $('#thumbnail-image').attr('default-image'));
        }
    });
    $('.plus-item').on('click', function() {
        var $clone = $('#defualt-item').clone();
        $clone.find('label').each(function() {
            $(this).attr('for', $(this).attr('for') + pdf_no);
        });
        $clone.find('input').each(function() {
            $(this).attr('id', $(this).attr('id') + pdf_no);
        });
        $clone.find('span').each(function() {
            $(this).attr('id', $(this).attr('id') + pdf_no);
        });
        $clone.find('.sr-no').html(`::${pdf_no}.`);
        $('#append-table').append($clone);
        $clone.find('[id^=audio-plain], [id^=pdf-]').on('change', function(event) {
            var $id = $(this).attr('id');
            if (this.files && this.files[0]) {
                $('#name-' + $id).html(this.files[0]['name']);
            } else {
                $('#name-' + $id).html('');
            }
        });
        $clone.find('.delete-pdf').on('click', function() {
            pdf_no--;
            $(this).closest('tr').remove();
        });
        pdf_no++;
    });
    $('form').on('submit', function() {
        $('#loader').show();
    })
});