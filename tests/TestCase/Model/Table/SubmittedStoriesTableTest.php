<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubmittedStoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubmittedStoriesTable Test Case
 */
class SubmittedStoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SubmittedStoriesTable
     */
    public $SubmittedStories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SubmittedStories',
        'app.Users',
        'app.Attachments',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SubmittedStories') ? [] : ['className' => SubmittedStoriesTable::class];
        $this->SubmittedStories = TableRegistry::getTableLocator()->get('SubmittedStories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubmittedStories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
