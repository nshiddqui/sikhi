<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HuqmnamasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HuqmnamasTable Test Case
 */
class HuqmnamasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HuqmnamasTable
     */
    public $Huqmnamas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Huqmnamas',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Huqmnamas') ? [] : ['className' => HuqmnamasTable::class];
        $this->Huqmnamas = TableRegistry::getTableLocator()->get('Huqmnamas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Huqmnamas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
