<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ScreenshotsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScreenshotsTable Test Case
 */
class ScreenshotsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ScreenshotsTable
     */
    public $Screenshots;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Screenshots',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Screenshots') ? [] : ['className' => ScreenshotsTable::class];
        $this->Screenshots = TableRegistry::getTableLocator()->get('Screenshots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Screenshots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
