<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SocialMediasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SocialMediasTable Test Case
 */
class SocialMediasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SocialMediasTable
     */
    public $SocialMedias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SocialMedias',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SocialMedias') ? [] : ['className' => SocialMediasTable::class];
        $this->SocialMedias = TableRegistry::getTableLocator()->get('SocialMedias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SocialMedias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
