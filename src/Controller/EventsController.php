<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Routing\Router;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 *
 * @method \App\Model\Entity\Event[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EventsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $conditions = [];
        if ($this->request->getQuery('date')) {
            $conditions['OR'][0]['Events.start_date <='] = $this->request->getQuery('date');
            $conditions['OR'][0]['Events.end_date >='] = $this->request->getQuery('date');
        }
        $this->DataTables->createConfig('Events')
                ->queryOptions([
                    'conditions' => $conditions
                ])
                ->options(['ajax' => ['data' => "%f%function(data){data.date = $('#date').val();}%f%"]])
                ->column('Events.id', ['label' => '#'])
                ->column('Events.image', ['label' => 'Image'])
                ->column('Events.name', ['label' => 'Name'])
                ->column('Events.start_date', ['label' => 'Start Date'])
                ->column('Events.end_date', ['label' => 'End Date'])
                ->column('Events.search_keywords', ['label' => 'Search Keywords'])
                ->column('Events.link', ['label' => 'Link Associated'])
                ->column('Events.created', ['label' => 'Date & Time'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Events);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Events');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $event = $this->Events->get($id, [
            'contain' => ['Events'],
        ]);

        $this->set('event', $event);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $event = $this->Events->newEntity();
        if ($this->request->is('post')) {
            $event = $this->Events->patchEntity($event, $this->getUserData());
            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $this->set(compact('event'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $event = $this->Events->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $event = $this->Events->patchEntity($event, $this->getUserData());
            if ($this->Events->save($event)) {
                $this->Flash->success(__('The event has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $this->set(compact('event'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Event id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $event = $this->Events->get($id);
        if ($this->Events->delete($event)) {
            $this->Flash->success(__('The event has been deleted.'));
        } else {
            $this->Flash->error(__('The event could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getUserData() {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        if (!empty($data['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('image');
            $data['image'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
        } else {
            unset($data['image']);
        }
        return $data;
    }

    public function upload() {
        $event = $this->Events->newEntity();
        if ($this->request->is('post')) {
            if (!empty($file_path = $this->request->getData('file')['tmp_name'])) {
                $this->loadComponent('Ics');
                $data = $this->Ics->load(file_get_contents($file_path));
                if (isset($data['VEVENT']) && !empty($data['VEVENT'])) {
                    foreach ($data['VEVENT'] as $eventData) {
                        $tmpData = [];
                        if (isset($eventData['UID']) && !empty($eventData['UID'])) {
                            $tmpData['event_id'] = $eventData['UID'];
                        }
                        if (isset($eventData['DTSTART']['value']) && !empty($eventData['DTSTART']['value'])) {
                            $tmpData['start_date'] = date('Y-m-d', strtotime($eventData['DTSTART']['value']));
                        }
                        if (isset($eventData['DTEND']['value']) && !empty($eventData['DTEND']['value'])) {
                            $tmpData['end_date'] = date('Y-m-d', strtotime($eventData['DTEND']['value']));
                        }
                        if (isset($eventData['DESCRIPTION']) && !empty($eventData['DESCRIPTION'])) {
                            $tmpData['description'] = $eventData['DESCRIPTION'];
                        }
                        if (isset($eventData['SUMMARY']) && !empty($eventData['SUMMARY'])) {
                            $tmpData['name'] = $eventData['SUMMARY'];
                        }
                        if (isset($eventData['CREATED']) && !empty($eventData['CREATED'])) {
                            $tmpData['created'] = date('Y-m-d h:i:s', strtotime($eventData['CREATED']));
                        }
                        if (isset($eventData['LAST-MODIFIED']) && !empty($eventData['LAST-MODIFIED'])) {
                            $tmpData['modified'] = date('Y-m-d h:i:s', strtotime($eventData['LAST-MODIFIED']));
                        }
                        $isAlreadyExist = $this->Events->find('all', [
                                    'conditions' => [
                                        'event_id' => $tmpData['event_id']
                                    ]
                                ])->first();

                        if ($isAlreadyExist == NULL) {
                            $event = $this->Events->newEntity();
                            $event = $this->Events->patchEntity($event, $tmpData);
                            $this->Events->save($event);
                        } else {
                            $isAlreadyExist = $this->Events->patchEntity($isAlreadyExist, $tmpData);
                            $this->Events->save($isAlreadyExist);
                        }
                    }
                    $this->Flash->success(__('The event has been uploaded.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error('Event Not Found.');
                }
            }
            $this->Flash->error(__('The event could not be saved. Please, try again.'));
        }
        $this->set(compact('event'));
    }

}
