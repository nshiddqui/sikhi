<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Screenshot;
use Cake\Routing\Router;

/**
 * Screenshots Controller
 *
 * @property \App\Model\Table\ScreenshotsTable $Screenshots
 *
 * @method \App\Model\Entity\Screenshot[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ScreenshotsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    public function index()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $error = false;
            foreach ($this->getScreenshotData() as $data) {
                $screenshot = $this->Screenshots->get($data['id'], [
                    'contain' => [],
                ]);
                $screenshot = $this->Screenshots->patchEntity($screenshot, $data);
                if (!$this->Screenshots->save($screenshot)) {
                    $error = true;
                }
            }
            if ($error === false) {
                $this->Flash->success(__('The screenshot has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The screenshot could not be saved. Please, try again.'));
        }
        $screenshot = $this->Screenshots->find('all', [
            'conditions' => [
                'id IN' => [1, 2, 3]
            ],
            'contain' => [],
        ]);

        $this->set(compact('screenshot'));
    }

    public function getScreenshotData()
    {
        $datas = $this->request->getData();
        $this->loadComponent('Fileupload');
        foreach ($datas as $key => $data) {
            if (!empty($data['image']['tmp_name'])) {
                $_FILES['image'] = $data['image'];
                $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
                $this->Fileupload->upload('image');
                $datas[$key]['image'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
            } else {
                unset($datas[$key]['image']);
            }
        }

        return $datas;
    }
}
