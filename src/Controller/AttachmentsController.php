<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Attachments Controller
 *
 * @property \App\Model\Table\AttachmentsTable $Attachments
 *
 * @method \App\Model\Entity\Attachment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AttachmentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['upload']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SubmittedStories'],
        ];
        $attachments = $this->paginate($this->Attachments);

        $this->set(compact('attachments'));
    }

    /**
     * View method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $attachment = $this->Attachments->get($id, [
            'contain' => ['SubmittedStories'],
        ]);

        $this->set('attachment', $attachment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $attachment = $this->Attachments->newEntity();
        if ($this->request->is('post')) {
            $attachment = $this->Attachments->patchEntity($attachment, $this->request->getData());
            if ($this->Attachments->save($attachment)) {
                $this->Flash->success(__('The attachment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attachment could not be saved. Please, try again.'));
        }
        $submittedStories = $this->Attachments->SubmittedStories->find('list', ['limit' => 200]);
        $this->set(compact('attachment', 'submittedStories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $attachment = $this->Attachments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attachment = $this->Attachments->patchEntity($attachment, $this->request->getData());
            if ($this->Attachments->save($attachment)) {
                $this->Flash->success(__('The attachment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attachment could not be saved. Please, try again.'));
        }
        $submittedStories = $this->Attachments->SubmittedStories->find('list', ['limit' => 200]);
        $this->set(compact('attachment', 'submittedStories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attachment = $this->Attachments->get($id);
        if ($this->Attachments->delete($attachment)) {
            $this->Flash->success(__('The attachment has been deleted.'));
        } else {
            $this->Flash->error(__('The attachment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function upload()
    {
        $this->request->allowMethod(['api']);
        $data = $this->request->getData();
        $fileType = [0, 1, 2, 3];
        if (!array_key_exists('type', $data) || !array_key_exists($data['type'], $fileType)) {
            $this->Flash->error('type field is required and it can be only these integer {0 = Image,1 = Video,2 = Audio, 3 = Pdf}');
            return;
        }
        $this->loadComponent('Fileupload');
        if (isset($_FILES) && !empty($_FILES)) {
            $data = [];
            foreach ($_FILES as $key => $files) {
                if (!empty($files['tmp_name'])) {
                    switch ($data['type']) {
                        case 0:
                            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
                            $this->Fileupload->upload('image');
                            $data[] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
                            break;
                        case 1:
                            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/video/', 'allowed_types' => ['mov', 'mp4', 'wmv', 'flv', 'avi', 'webm', 'flv', 'mkv'], 'encrypt_name' => true]);
                            $this->Fileupload->upload('image');
                            $data[] = Router::url(['controller' => 'files', 'action' => 'video', $this->Fileupload->output('file_name')], true);
                            break;
                        case 2:
                            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/audio/', 'allowed_types' => ['mp3'], 'encrypt_name' => true]);
                            $this->Fileupload->upload('image');
                            $data[] = Router::url(['controller' => 'files', 'action' => 'audio', $this->Fileupload->output('file_name')], true);
                            break;
                        case 3:
                            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/pdf/', 'allowed_types' => ['pdf'], 'encrypt_name' => true]);
                            $this->Fileupload->upload('image');
                            $data[] = Router::url(['controller' => 'files', 'action' => 'pdf', $this->Fileupload->output('file_name')], true);
                            break;
                    }
                }
            }
            $this->set(compact('data'));
        } else {
            $this->Flash->error('No File Available');
        }
    }
}
