<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Routing\Router;

ini_set('upload_max_filesize', '10M');
ini_set('post_max_size', '10M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

/**
 * Stories Controller
 *
 * @property \App\Model\Table\StoriesTable $Stories
 *
 * @method \App\Model\Entity\Story[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StoriesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadModel('DropDown');
        $age_group = $this->DropDown->getAgeGroup();
        $type = $this->DropDown->getStoryType();
        $this->set(compact('age_group', 'type'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Stories')
                ->column('Stories.id', ['label' => '#'])
                ->column('Stories.name', ['label' => 'Story Title'])
                //->column('Stories.type', ['label' => 'Article Type'])
                ->column('Stories.age_group', ['label' => 'Age Group'])
                ->column('Stories.created', ['label' => 'Publish Date/Time'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Stories);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Stories');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $story = $this->Stories->newEntity();
        if ($this->request->is('post')) {
            $story = $this->Stories->patchEntity($story, $this->getStoryData());
            if ($this->Stories->save($story)) {
                $this->Flash->success(__('The story has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The story could not be saved. Please, try again.'));
        }
        $this->set(compact('story'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Story id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $type = 0) {
        $story = $this->Stories->get($id, [
            'contain' => ['Pdfs'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $story = $this->Stories->patchEntity($story, $this->getStoryData());
            if ($this->Stories->save($story)) {
                $this->Flash->success(__('The story has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The story could not be saved. Please, try again.'));
        }
        $story->type = $type;
        $this->set(compact('story'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Story id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $story = $this->Stories->get($id);
        if ($this->Stories->delete($story)) {
            $this->Flash->success(__('The story has been deleted.'));
        } else {
            $this->Flash->error(__('The story could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteFile($type, $id) {
        if (in_array($type, ['audio_plain_english', 'audio_plain_punjabi', 'video_english', 'video_punjabi', 'audio_english', 'audio_punjabi'])) {
            $story = $this->Stories->get($id);
            $story = $this->Stories->patchEntity($story, [$type => null]);
            if ($this->Stories->save($story)) {
                $this->Flash->success(__('The file has been deleted.'));
            } else {
                $this->Flash->error(__('The file could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect($this->referer());
    }

    public function deletePdfFile($type, $id = null) {
        if (is_numeric($type)) {
            $id = $type;
        }
        if (in_array($type, ['english', 'punjabi', 'audio_english', 'audio_punjabi'])) {
            $storyPdf = $this->Stories->Pdfs->get($id);
            $storyPdf = $this->Stories->Pdfs->patchEntity($storyPdf, [$type => null]);
            if ($this->Stories->Pdfs->save($storyPdf)) {
                $this->Flash->success(__('The file has been deleted.'));
            } else {
                $this->Flash->error(__('The file could not be deleted. Please, try again.'));
            }
        } else {
            $storyPdf = $this->Stories->Pdfs->get($id);
            if ($this->Stories->Pdfs->delete($storyPdf)) {
                $this->Flash->success(__('The file has been deleted.'));
            } else {
                $this->Flash->error(__('The file could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'edit', $storyPdf->story_id, 1]);
    }

    private function getStoryData() {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        //if ($data['type'] == 0) {
        foreach (['audio_plain_english', 'audio_plain_punjabi'] as $variable) {
            if (!empty($data[$variable]['tmp_name'])) {
                $data[$variable] = \Cloudinary\Uploader::upload($data[$variable]["tmp_name"], array("resource_type" => "auto"))['secure_url'];
            } else {
                unset($data[$variable]);
            }
        }
        //} else {
        //    foreach (['audio_plain_english', 'audio_plain_punjabi'] as $variable) {
        //        unset($data[$variable]);
        //    }
        //}
        //if ($data['type'] == 1) {
        $data['pdfs'] = [];
        foreach ($data['pdf']['english'] as $key => $value) {
            $pdf = [];
            foreach (['english', 'punjabi'] as $variable) {
                if (!empty($data['pdf'][$variable][$key]['tmp_name'])) {
                    $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/pdf/', 'allowed_types' => 'pdf', 'encrypt_name' => true]);
                    $_FILES['pdf_file'] = $data['pdf'][$variable][$key];
                    $this->Fileupload->upload('pdf_file');
                    $pdf[$variable] = Router::url(['controller' => 'files', 'action' => 'pdf', $this->Fileupload->output('file_name')], true);
                }
            }
            foreach (['audio_english', 'audio_punjabi'] as $variable) {
                if (!empty($data['pdf'][$variable][$key]['tmp_name'])) {
                    $pdf[$variable] = \Cloudinary\Uploader::upload($data['pdf'][$variable][$key]["tmp_name"], array("resource_type" => "auto"))['secure_url'];
                }
            }
            if (!empty($pdf)) {
                if (isset($data['pdf']['id'][$key]) && !empty($data['pdf']['id'][$key])) {
                    $pdf['id'] = $data['pdf']['id'][$key];
                }
                array_push($data['pdfs'], $pdf);
            }
        }
        //}
        unset($data['pdf']);
        //if ($data['type'] == 2) {
        foreach (['video_english', 'video_punjabi'] as $variable) {
            if (!empty($data[$variable]['tmp_name'])) {
                $data[$variable] = \Cloudinary\Uploader::upload($data[$variable]["tmp_name"], array("resource_type" => "auto"))['secure_url'];
            } else {
                $data[$variable] = $data[$variable . '_link'];
            }
        }
        //} else {
        //    foreach (['video_english', 'video_punjabi'] as $variable) {
        //        unset($data[$variable]);
        //    }
        //}
        //if ($data['type'] == 3) {
        foreach (['audio_english', 'audio_punjabi'] as $variable) {
            if (!empty($data[$variable]['tmp_name'])) {
                $data[$variable] = \Cloudinary\Uploader::upload($data[$variable]["tmp_name"], array("resource_type" => "auto"))['secure_url'];
            } else {
                unset($data[$variable]);
            }
        }
        //} else {
        //    foreach (['audio_english', 'audio_punjabi'] as $variable) {
        //        unset($data[$variable]);
        //    }
        //}
        if (!empty($data['image']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('image');
            $data['image'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
        } else {
            unset($data['image']);
        }
        return $data;
    }

}
