<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Routing\Router;

/**
 * Medias Controller
 *
 * @property \App\Model\Table\MediasTable $Medias
 *
 * @method \App\Model\Entity\Media[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MediasController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('DropDown');
        $media_type = $this->DropDown->getMediaType();
        $this->set(compact('media_type'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Medias')
            ->column('Medias.id', ['label' => '#'])
            ->column('Medias.title', ['label' => 'Title'])
            ->column('Medias.type', ['label' => 'Media Type'])
            ->column('Medias.media', ['label' => 'Media Link'])
            ->column('Medias.thumbnail', ['label' => 'Media Thumbnail'])
            ->column('Medias.created', ['label' => 'Date & Time'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Medias);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Medias');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $media = $this->Medias->get($id, [
            'contain' => [],
        ]);

        $this->set('media', $media);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $media = $this->Medias->newEntity();
        if ($this->request->is('post')) {
            $media = $this->Medias->patchEntity($media, $this->getMediaData());
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media could not be saved. Please, try again.'));
        }
        $this->set(compact('media'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $media = $this->Medias->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $media = $this->Medias->patchEntity($media, $this->getMediaData());
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media could not be saved. Please, try again.'));
        }
        $this->set(compact('media'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $media = $this->Medias->get($id);
        if ($this->Medias->delete($media)) {
            $this->Flash->success(__('The media has been deleted.'));
        } else {
            $this->Flash->error(__('The media could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getMediaData()
    {
        $data = $this->request->getData();
        $this->loadComponent('Fileupload');
        if (!empty($data['thumbnail']['tmp_name'])) {
            $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
            $this->Fileupload->upload('thumbnail');
            $data['thumbnail'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
        } else {
            unset($data['thumbnail']);
        }

        if (!empty($data['media']['tmp_name'])) {
            if ($data['type'] == 0) {
                $this->Fileupload->init(['upload_path' => WWW_ROOT . 'files/img/', 'allowed_types' => ['bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'jp2', 'j2k', 'jpf', 'jpg2', 'jpx', 'jpm', 'mj2', 'mjp2', 'png', 'tiff', 'tif'], 'encrypt_name' => true]);
                $this->Fileupload->upload('media');
                $data['media'] = Router::url(['controller' => 'files', 'action' => 'img', $this->Fileupload->output('file_name')], true);
            } else {
                $data['media'] = \Cloudinary\Uploader::upload($data['media']["tmp_name"], array("resource_type" => "auto"))['secure_url'];
            }
        } else {
            if(!empty($data['link'])){
                $data['media'] = $data['link'];
            } else {
                unset($data['media']);
            }
        }
        unset($data['link']);
        return $data;
    }
}
