<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pdfs Controller
 *
 * @property \App\Model\Table\PdfsTable $Pdfs
 *
 * @method \App\Model\Entity\Pdf[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PdfsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stories'],
        ];
        $pdfs = $this->paginate($this->Pdfs);

        $this->set(compact('pdfs'));
    }

    /**
     * View method
     *
     * @param string|null $id Pdf id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pdf = $this->Pdfs->get($id, [
            'contain' => ['Stories'],
        ]);

        $this->set('pdf', $pdf);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pdf = $this->Pdfs->newEntity();
        if ($this->request->is('post')) {
            $pdf = $this->Pdfs->patchEntity($pdf, $this->request->getData());
            if ($this->Pdfs->save($pdf)) {
                $this->Flash->success(__('The pdf has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pdf could not be saved. Please, try again.'));
        }
        $stories = $this->Pdfs->Stories->find('list', ['limit' => 200]);
        $this->set(compact('pdf', 'stories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Pdf id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pdf = $this->Pdfs->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pdf = $this->Pdfs->patchEntity($pdf, $this->request->getData());
            if ($this->Pdfs->save($pdf)) {
                $this->Flash->success(__('The pdf has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pdf could not be saved. Please, try again.'));
        }
        $stories = $this->Pdfs->Stories->find('list', ['limit' => 200]);
        $this->set(compact('pdf', 'stories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pdf id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pdf = $this->Pdfs->get($id);
        if ($this->Pdfs->delete($pdf)) {
            $this->Flash->success(__('The pdf has been deleted.'));
        } else {
            $this->Flash->error(__('The pdf could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
