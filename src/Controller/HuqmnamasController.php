<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;

/**
 * Huqmnamas Controller
 *
 * @property \App\Model\Table\HuqmnamasTable $Huqmnamas
 *
 * @method \App\Model\Entity\Huqmnama[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HuqmnamasController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('Huqmnamas')
                ->column('Huqmnamas.id', ['label' => '#'])
                ->column('Huqmnamas.title', ['label' => 'Title'])
                ->column('Huqmnamas.text', ['label' => 'Text'])
                ->column('Huqmnamas.daily_katha_link', ['label' => 'Daily Katha Link'])
                ->column('Huqmnamas.date', ['label' => 'Date'])
                ->column('Huqmnamas.created', ['label' => 'Date & Time'])
                ->column('actions', ['label' => 'Actions', 'database' => false]);
    }

    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->Huqmnamas);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('Huqmnamas');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Huqmnama id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $huqmnama = $this->Huqmnamas->get($id, [
            'contain' => [],
        ]);

        $this->set('huqmnama', $huqmnama);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $huqmnama = $this->Huqmnamas->newEntity();
        if ($this->request->is('post')) {
            $huqmnama = $this->Huqmnamas->patchEntity($huqmnama, $this->request->getData());
            if ($this->Huqmnamas->save($huqmnama)) {
                $this->Flash->success(__('The huqmnama has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huqmnama could not be saved. Please, try again.'));
        }
        $this->set(compact('huqmnama'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Huqmnama id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $huqmnama = $this->Huqmnamas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $huqmnama = $this->Huqmnamas->patchEntity($huqmnama, $this->request->getData());
            if ($this->Huqmnamas->save($huqmnama)) {
                $this->Flash->success(__('The huqmnama has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The huqmnama could not be saved. Please, try again.'));
        }
        $this->set(compact('huqmnama'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Huqmnama id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $huqmnama = $this->Huqmnamas->get($id);
        if ($this->Huqmnamas->delete($huqmnama)) {
            $this->Flash->success(__('The huqmnama has been deleted.'));
        } else {
            $this->Flash->error(__('The huqmnama could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
