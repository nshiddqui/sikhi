<?php

namespace App\Controller;

use App\Controller\AppController;
use DataTables\Controller\DataTablesAjaxRequestTrait;
use Cake\Http\Client;

/**
 * SubmittedStories Controller
 *
 * @property \App\Model\Table\SubmittedStoriesTable $SubmittedStories
 *
 * @method \App\Model\Entity\SubmittedStory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubmittedStoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('DropDown');
        $age_groups = $this->DropDown->getAgeGroup();
        $submitted_story_status = $this->DropDown->getSubmittedStoryStatus();
        $this->set('imagePath', 'http://ijyaweb.com/sikhi_api/submitted_stories/');
        $this->set(compact('age_groups', 'submitted_story_status'));
        $this->loadComponent('DataTables.DataTables');
        $this->DataTables->createConfig('SubmittedStories')
            ->column('SubmittedStories.id', ['label' => '#'])
            ->column('SubmittedStories.image', ['label' => 'Image'])
            ->column('SubmittedStories.title', ['label' => 'Title'])
            ->column('SubmittedStories.user_name', ['label' => 'Full Name'])
            ->column('SubmittedStories.user_email', ['label' => 'Email'])
            ->column('SubmittedStories.user_phone', ['label' => 'Mobile'])
            ->column('SubmittedStories.age_group', ['label' => 'Age Group'])
            ->column('SubmittedStories.created', ['label' => 'Date & Time'])
            ->column('SubmittedStories.status', ['label' => 'Status'])
            ->column('actions', ['label' => 'Actions', 'database' => false]);
    }
    /*
     * User DataTable Ajax Request Trait
     */
    use DataTablesAjaxRequestTrait;

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->request->is('api')) {
            $data = $this->paginate($this->SubmittedStories);
            $this->set(compact('data'));
        } else {
            $this->DataTables->setViewVars('SubmittedStories');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Submitted Story id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $submittedStory = $this->SubmittedStories->get($id, [
            'contain' => ['Attachments'],
        ]);
        $this->DropDown->addMediaType(3, 'Pdf');
        $media_types = $this->DropDown->getMediaType();

        $this->set('submittedStory', $submittedStory);
        $this->set('media_types', $media_types);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $submittedStory = $this->SubmittedStories->newEntity();
        if ($this->request->is('post')) {
            $submittedStory = $this->SubmittedStories->patchEntity($submittedStory, $this->request->getData());
            if ($this->SubmittedStories->save($submittedStory)) {
                $this->Flash->success(__('The submitted story has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submitted story could not be saved. Please, try again.'));
        }
        $this->set(compact('submittedStory'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Submitted Story id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $submittedStory = $this->SubmittedStories->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submittedStory = $this->SubmittedStories->patchEntity($submittedStory, $this->request->getData());
            if ($this->SubmittedStories->save($submittedStory)) {
                $this->Flash->success(__('The submitted story has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submitted story could not be saved. Please, try again.'));
        }
        $this->set(compact('submittedStory'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Submitted Story id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submittedStory = $this->SubmittedStories->get($id, ['contain' => ['Attachments']]);
        if ($this->SubmittedStories->delete($submittedStory)) {
            $http = new Client();
            foreach ($submittedStory['attachments'] as $submittedStoryData) {
                $http->post('http://ijyaweb.com/sikhi_api/api/deleteArticlesFile', [
                    'path' => $submittedStoryData->attachment
                ]);
            }
            $this->Flash->success(__('The submitted story has been deleted.'));
        } else {
            $this->Flash->error(__('The submitted story could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function approve($id = null)
    {
        $this->loadModel('Notifications');
        $this->request->allowMethod(['post', 'approve']);
        $submittedStory = $this->SubmittedStories->get($id);
        $submittedStory->status = $this->request->getData('status_id');
        $submitted_story_status = $this->DropDown->getSubmittedStoryStatus();
        if ($this->SubmittedStories->save($submittedStory)) {
            $notification = $this->Notifications->newEntity();
            $notification = $this->Notifications->patchEntity($notification, [
                'user_id' => $submittedStory->user_id,
                'age_group' => $submittedStory->age_group,
                'title' => "Status Update Submitted Article (No. {$submittedStory->id})",
                'detail' => "Status Update Submitted Article (No. {$submittedStory->id}) To {$submitted_story_status[$submittedStory->status]}",
            ]);
            $this->Notifications->save($notification);
            $this->Flash->success(__("The submitted story has been {$submitted_story_status[$submittedStory->status]}."));
        } else {
            $this->Flash->error(__("The submitted story could not be {$submitted_story_status[$submittedStory->status]}. Please, try again."));
        }

        return $this->redirect(['action' => 'index']);
    }
}
