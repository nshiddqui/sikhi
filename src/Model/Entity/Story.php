<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Story Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property string $meta_data
 * @property string $keywords
 * @property int $age_group
 * @property int $type
 * @property string|null $plain_english
 * @property string|null $plain_punjabi
 * @property string|null $audio_plain_english
 * @property string|null $audio_plain_punjabi
 * @property string|null $video_english
 * @property string|null $video_punjabi
 * @property string|null $audio_english
 * @property string|null $audio_punjabi
 * @property int $view_count
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Pdf[] $pdfs
 */
class Story extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'image' => true,
        'meta_data' => true,
        'keywords' => true,
        'age_group' => true,
        //'type' => true,
        'plain_english' => true,
        'plain_punjabi' => true,
        'audio_plain_english' => true,
        'audio_plain_punjabi' => true,
        'video_english' => true,
        'video_punjabi' => true,
        'audio_english' => true,
        'audio_punjabi' => true,
        'view_count' => true,
        'created' => true,
        'modified' => true,
        'pdfs' => true,
    ];
}
