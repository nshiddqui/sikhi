<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubmittedStory Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $user_name
 * @property string|null $user_email
 * @property string|null $user_phone
 * @property string|null $image
 * @property string $title
 * @property int $age_group
 * @property string|null $description
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Attachment[] $attachments
 */
class SubmittedStory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'user_name' => true,
        'user_email' => true,
        'user_phone' => true,
        'image' => true,
        'title' => true,
        'age_group' => true,
        'description' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'attachments' => true,
    ];
}
