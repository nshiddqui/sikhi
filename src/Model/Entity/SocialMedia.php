<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SocialMedia Entity
 *
 * @property int $id
 * @property string|null $about_us
 * @property string|null $fb_link
 * @property string|null $twitter_link
 * @property string|null $website_link
 * @property string|null $instagram_link
 * @property string|null $linkedin_link
 * @property string|null $daily_katha_link
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class SocialMedia extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'about_us' => true,
        'fb_link' => true,
        'twitter_link' => true,
        'website_link' => true,
        'instagram_link' => true,
        'linkedin_link' => true,
        'created' => true,
        'modified' => true,
    ];
}
