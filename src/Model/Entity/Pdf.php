<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pdf Entity
 *
 * @property int $id
 * @property int $story_id
 * @property string|null $english
 * @property string|null $punjabi
 * @property string|null $audio_english
 * @property string|null $audio_punjabi
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Story $story
 */
class Pdf extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'story_id' => true,
        'english' => true,
        'punjabi' => true,
        'audio_english' => true,
        'audio_punjabi' => true,
        'created' => true,
        'modified' => true,
        'story' => true,
    ];
}
