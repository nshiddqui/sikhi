<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $image
 * @property int $age_group
 * @property string|null $device
 * @property string|null $push_token
 * @property string|null $device_model
 * @property string|null $country
 * @property \Cake\I18n\FrozenTime|null $last_open_time
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'image' => true,
        'age_group' => true,
        'device' => true,
        'push_token' => true,
        'device_model' => true,
        'country' => true,
        'last_open_time' => true,
        'created' => true,
        'modified' => true,
    ];
}
