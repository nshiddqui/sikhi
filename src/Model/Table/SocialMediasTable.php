<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SocialMedias Model
 *
 * @method \App\Model\Entity\SocialMedia get($primaryKey, $options = [])
 * @method \App\Model\Entity\SocialMedia newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SocialMedia[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SocialMedia saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SocialMedia patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SocialMedia findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SocialMediasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('social_medias');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('about_us')
            ->allowEmptyString('about_us');

        $validator
            ->scalar('fb_link')
            ->maxLength('fb_link', 255)
            ->allowEmptyString('fb_link');

        $validator
            ->scalar('twitter_link')
            ->maxLength('twitter_link', 255)
            ->allowEmptyString('twitter_link');

        $validator
            ->scalar('website_link')
            ->maxLength('website_link', 255)
            ->allowEmptyString('website_link');

        $validator
            ->scalar('instagram_link')
            ->maxLength('instagram_link', 255)
            ->allowEmptyString('instagram_link');

        $validator
            ->scalar('linkedin_link')
            ->maxLength('linkedin_link', 255)
            ->allowEmptyString('linkedin_link');


        return $validator;
    }
}
