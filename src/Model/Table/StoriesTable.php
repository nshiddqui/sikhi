<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stories Model
 *
 * @property \App\Model\Table\PdfsTable&\Cake\ORM\Association\HasMany $Pdfs
 *
 * @method \App\Model\Entity\Story get($primaryKey, $options = [])
 * @method \App\Model\Entity\Story newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Story[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Story|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Story saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Story patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Story[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Story findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Pdfs', [
            'foreignKey' => 'story_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmptyFile('image');

        $validator
            ->scalar('meta_data')
            ->requirePresence('meta_data', 'create')
            ->notEmptyString('meta_data');

        $validator
            ->scalar('keywords')
            ->maxLength('keywords', 255)
            ->requirePresence('keywords', 'create')
            ->notEmptyString('keywords');

        $validator
            ->integer('age_group')
            ->requirePresence('age_group', 'create')
            ->notEmptyString('age_group');

        //$validator
        //    ->integer('type')
        //    ->requirePresence('type', 'create')
        //    ->notEmptyString('type');

        $validator
            ->scalar('plain_english')
            ->allowEmptyString('plain_english');

        $validator
            ->scalar('plain_punjabi')
            ->allowEmptyString('plain_punjabi');

        $validator
            ->scalar('audio_plain_english')
            ->maxLength('audio_plain_english', 255)
            ->allowEmptyString('audio_plain_english');

        $validator
            ->scalar('audio_plain_punjabi')
            ->maxLength('audio_plain_punjabi', 255)
            ->allowEmptyString('audio_plain_punjabi');

        $validator
            ->scalar('video_english')
            ->maxLength('video_english', 255)
            ->allowEmptyString('video_english');

        $validator
            ->scalar('video_punjabi')
            ->maxLength('video_punjabi', 255)
            ->allowEmptyString('video_punjabi');

        $validator
            ->scalar('audio_english')
            ->maxLength('audio_english', 255)
            ->allowEmptyString('audio_english');

        $validator
            ->scalar('audio_punjabi')
            ->maxLength('audio_punjabi', 255)
            ->allowEmptyString('audio_punjabi');

        $validator
            ->integer('view_count')
            ->notEmptyString('view_count');

        return $validator;
    }
}
