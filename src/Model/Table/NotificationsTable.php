<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;

/**
 * Notifications Model
 *
 * @method \App\Model\Entity\Notification get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notification findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NotificationsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('notifications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->integer('age_group')
                ->allowEmptyString('age_group');

        $validator
                ->integer('user_id')
                ->allowEmptyString('user_id');

        $validator
                ->scalar('title')
                ->maxLength('title', 255)
                ->requirePresence('title', 'create')
                ->notEmptyString('title');

        $validator
                ->scalar('detail')
                ->notEmptyString('detail');

        $validator
                ->scalar('link')
                ->maxLength('link', 255)
                ->allowEmptyString('link');

        return $validator;
    }

    protected function _insert($entity, $data) {
        $client = new Client();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $conditition = [];
        if (array_key_exists('age_group', $data) && !empty($data['age_group'])) {
            $conditition['age_group'] = $data['age_group'];
        }
        $this->Users->setDisplayField('push_token');
        $tokens = $this->Users->find('list', ['conditions' => $conditition])->toArray();
        try {
            $client->get('http://ijyaweb.com/bingalo_api/api/sendNotification/' . json_encode($tokens) . '/' . $data['title']);
        } catch (\Exception $e) {
            
        }
        return parent::_insert($entity, $data);
    }

}
