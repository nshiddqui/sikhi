<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class DropDownTable extends Table
{

    public $age_group = [
        'Children',
        'Teens',
        'Adults',
        'Elderly'
    ];

    public $story_type = [
        'Plain Text',
        'Pdf',
        'Video',
    ];

    public $media_type = [
        'Image',
        'Video',
        'Audio'
    ];

    public $submited_story_status = [
        'Awaiting',
        'Approved',
        'Declined'
    ];


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable(false);
    }

    public function getAgeGroup($key = null)
    {
        if (is_null($key)) {
            return $this->age_group;
        } else {
            return $this->age_group[$key];
        }
    }

    public function addAgeGroup($key, $value)
    {
        $this->age_group[$key] = $value;
    }

    public function removeAgeGroup($value)
    {
        if ($key = array_search($value, $this->age_group)) {
            unset($this->age_group[$key]);
        }
    }

    public function getStoryType($key = null)
    {
        if (is_null($key)) {
            return $this->story_type;
        } else {
            return $this->story_type[$key];
        }
    }

    public function addStoryType($key, $value)
    {
        $this->story_type[$key] = $value;
    }

    public function removeStoryType($value)
    {
        if ($key = array_search($value, $this->story_type)) {
            unset($this->story_type[$key]);
        }
    }

    public function getMediaType($key = null)
    {
        if (is_null($key)) {
            return $this->media_type;
        } else {
            return $this->media_type[$key];
        }
    }

    public function addMediaType($key, $value)
    {
        $this->media_type[$key] = $value;
    }

    public function removeMediaType($value)
    {
        if ($key = array_search($value, $this->media_type)) {
            unset($this->media_type[$key]);
        }
    }

    public function getSubmittedStoryStatus($key = null)
    {
        if (is_null($key)) {
            return $this->submited_story_status;
        } else {
            return $this->submited_story_status[$key];
        }
    }

    public function addSubmittedStoryStatus($key, $value)
    {
        $this->submited_story_status[$key] = $value;
    }

    public function removeSubmittedStoryStatus($value)
    {
        if ($key = array_search($value, $this->submited_story_status)) {
            unset($this->submited_story_status[$key]);
        }
    }
}
