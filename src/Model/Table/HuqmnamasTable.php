<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Huqmnamas Model
 *
 * @method \App\Model\Entity\Huqmnama get($primaryKey, $options = [])
 * @method \App\Model\Entity\Huqmnama newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Huqmnama[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Huqmnama|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Huqmnama saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Huqmnama patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Huqmnama[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Huqmnama findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HuqmnamasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('huqmnamas');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('text')
            ->allowEmptyString('text');
        
        $validator
            ->scalar('daily_katha_link')
            ->maxLength('daily_katha_link', 255)
            ->allowEmptyString('daily_katha_link');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmptyDate('date');

        return $validator;
    }

    protected function _insert($entity, $data)
    {
        $notifications_table = TableRegistry::getTableLocator()->get('Notifications');
        $notifications = $notifications_table->newEntity([
            'title' => $data['title'],
            'detail' => array_key_exists('text', $data) ? $data['text'] : ''
        ]);
        $notifications_table->save($notifications);

        return parent::_insert($entity, $data);
    }
}
