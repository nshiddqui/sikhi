<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Notifications</h3>
        <?= $this->Html->link('Add Notification', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-warning pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Notifications') ?>
    </div>
</div>