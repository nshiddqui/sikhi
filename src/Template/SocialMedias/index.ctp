<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SocialMedia[]|\Cake\Collection\CollectionInterface $socialMedias
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Social Media'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="socialMedias index large-9 medium-8 columns content">
    <h3><?= __('Social Medias') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fb_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('twitter_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('website_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('instagram_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('linkedin_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('daily_katha_link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($socialMedias as $socialMedia): ?>
            <tr>
                <td><?= $this->Number->format($socialMedia->id) ?></td>
                <td><?= h($socialMedia->fb_link) ?></td>
                <td><?= h($socialMedia->twitter_link) ?></td>
                <td><?= h($socialMedia->website_link) ?></td>
                <td><?= h($socialMedia->instagram_link) ?></td>
                <td><?= h($socialMedia->linkedin_link) ?></td>
                <td><?= h($socialMedia->daily_katha_link) ?></td>
                <td><?= h($socialMedia->created) ?></td>
                <td><?= h($socialMedia->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $socialMedia->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $socialMedia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $socialMedia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $socialMedia->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
