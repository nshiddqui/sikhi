<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SocialMedia $socialMedia
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Social Medias'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="socialMedias form large-9 medium-8 columns content">
    <?= $this->Form->create($socialMedia) ?>
    <fieldset>
        <legend><?= __('Add Social Media') ?></legend>
        <?php
            echo $this->Form->control('about_us');
            echo $this->Form->control('fb_link');
            echo $this->Form->control('twitter_link');
            echo $this->Form->control('website_link');
            echo $this->Form->control('instagram_link');
            echo $this->Form->control('linkedin_link');
            echo $this->Form->control('daily_katha_link');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
