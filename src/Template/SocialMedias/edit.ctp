<?= $this->Form->create($socialMedia) ?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Social Media Managements') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?php
        echo $this->Form->control('about_us');
        echo $this->Form->control('fb_link', ['type' => 'url']);
        echo $this->Form->control('twitter_link', ['type' => 'url']);
        echo $this->Form->control('website_link', ['type' => 'url']);
        echo $this->Form->control('instagram_link', ['type' => 'url']);
        echo $this->Form->control('linkedin_link', ['type' => 'url']);
        ?>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <?= $this->Form->button(__('Update')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>