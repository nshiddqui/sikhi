<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SocialMedia $socialMedia
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Social Media'), ['action' => 'edit', $socialMedia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Social Media'), ['action' => 'delete', $socialMedia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $socialMedia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Social Medias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Social Media'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="socialMedias view large-9 medium-8 columns content">
    <h3><?= h($socialMedia->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Fb Link') ?></th>
            <td><?= h($socialMedia->fb_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Twitter Link') ?></th>
            <td><?= h($socialMedia->twitter_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Website Link') ?></th>
            <td><?= h($socialMedia->website_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Instagram Link') ?></th>
            <td><?= h($socialMedia->instagram_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Linkedin Link') ?></th>
            <td><?= h($socialMedia->linkedin_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Daily Katha Link') ?></th>
            <td><?= h($socialMedia->daily_katha_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($socialMedia->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($socialMedia->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($socialMedia->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('About Us') ?></h4>
        <?= $this->Text->autoParagraph(h($socialMedia->about_us)); ?>
    </div>
</div>
