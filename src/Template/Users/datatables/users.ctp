<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        $this->Html->image((!empty($result->image) ? $imagePath . $result->image : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle']),
        h($result->name),
        h($result->email),
        h($result->created),
        $this->Html->link($this->Html->image('view.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'view', $result->id], ['escape' => false])
    ]);
}
echo $this->DataTables->response();
