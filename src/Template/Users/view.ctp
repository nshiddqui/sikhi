<div class="box-body">
    <div class="row">
        <div class="col-md-3">&nbsp;</div>
        <div class="col-md-6">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active text-center">
                    <?= $this->Html->image((!empty($user['image']) ? $imagePath . $user['image'] : 'not-found.png'), ['class' => 'img-circle', 'style' => 'height:110px']) ?>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href='javascript:void(0)'><strong>User ID</strong><span class="pull-right"><?= $user['id'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Full Name</strong><span class="pull-right"><?= $user['name'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Email</strong><span class="pull-right"><?= $user['email'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Age Group</strong><span class="pull-right"><?= $age_group[$user['age_group']] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Country</strong><span class="pull-right"><?= $user['country'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Device Name</strong><span class="pull-right"><?= $user['device'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Device Model</strong><span class="pull-right"><?= $user['device_model'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Push Token</strong><span class="pull-right"><?= $user['push_token'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Last Open Time</strong><span class="pull-right"><?= $user['last_open_time'] ?></span></a></li>
                        <li><a href='javascript:void(0)'><strong>Registration Date & Time</strong><span class="pull-right"><?= $user['created'] ?></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>