<?php
$this->assign('title', 'User Management');
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Users</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Users') ?>
    </div>
</div>