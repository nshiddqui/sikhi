<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pdf $pdf
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pdf'), ['action' => 'edit', $pdf->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pdf'), ['action' => 'delete', $pdf->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pdf->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pdfs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pdf'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stories'), ['controller' => 'Stories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Story'), ['controller' => 'Stories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pdfs view large-9 medium-8 columns content">
    <h3><?= h($pdf->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Story') ?></th>
            <td><?= $pdf->has('story') ? $this->Html->link($pdf->story->name, ['controller' => 'Stories', 'action' => 'view', $pdf->story->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('English') ?></th>
            <td><?= h($pdf->english) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Punjabi') ?></th>
            <td><?= h($pdf->punjabi) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Audio English') ?></th>
            <td><?= h($pdf->audio_english) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Audio Punjabi') ?></th>
            <td><?= h($pdf->audio_punjabi) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($pdf->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($pdf->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($pdf->modified) ?></td>
        </tr>
    </table>
</div>
