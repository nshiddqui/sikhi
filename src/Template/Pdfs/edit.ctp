<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pdf $pdf
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pdf->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pdf->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pdfs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Stories'), ['controller' => 'Stories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Story'), ['controller' => 'Stories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pdfs form large-9 medium-8 columns content">
    <?= $this->Form->create($pdf) ?>
    <fieldset>
        <legend><?= __('Edit Pdf') ?></legend>
        <?php
            echo $this->Form->control('story_id', ['options' => $stories]);
            echo $this->Form->control('english');
            echo $this->Form->control('punjabi');
            echo $this->Form->control('audio_english');
            echo $this->Form->control('audio_punjabi');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
