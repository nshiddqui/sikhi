<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pdf[]|\Cake\Collection\CollectionInterface $pdfs
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pdf'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Stories'), ['controller' => 'Stories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Story'), ['controller' => 'Stories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pdfs index large-9 medium-8 columns content">
    <h3><?= __('Pdfs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('story_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('english') ?></th>
                <th scope="col"><?= $this->Paginator->sort('punjabi') ?></th>
                <th scope="col"><?= $this->Paginator->sort('audio_english') ?></th>
                <th scope="col"><?= $this->Paginator->sort('audio_punjabi') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pdfs as $pdf): ?>
            <tr>
                <td><?= $this->Number->format($pdf->id) ?></td>
                <td><?= $pdf->has('story') ? $this->Html->link($pdf->story->name, ['controller' => 'Stories', 'action' => 'view', $pdf->story->id]) : '' ?></td>
                <td><?= h($pdf->english) ?></td>
                <td><?= h($pdf->punjabi) ?></td>
                <td><?= h($pdf->audio_english) ?></td>
                <td><?= h($pdf->audio_punjabi) ?></td>
                <td><?= h($pdf->created) ?></td>
                <td><?= h($pdf->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pdf->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pdf->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pdf->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pdf->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
