<?= $this->Form->create($huqmnama) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add/Edit Hukamnama') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?php
        echo $this->Form->control('title');
        echo $this->Form->control('text');
        echo $this->Form->control('date', ['type' => 'text', 'datepicker' => true]);
        echo $this->Form->control('daily_katha_link', ['type' => 'url']);
        ?>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>