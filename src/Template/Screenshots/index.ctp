<?= $this->html->css('screenshot', ['block' => true]) ?>
<?= $this->html->script('screenshot', ['block' => true]) ?>
<?= $this->Form->create(null, ['type' => 'file']) ?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Screenshot Managements') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <?php foreach ($screenshot as $screen_shot) { ?>
                <div class="col-md-4">
                    <h3 class="text-center">Screen <?= $screen_shot->id ?></h3>
                    <?= $this->Form->hidden($screen_shot->id . '.id', ['value' => $screen_shot->id]) ?>
                    <?= $this->Form->control($screen_shot->id . '.title', ['label' => false, 'value' => $screen_shot->title]) ?>
                    <?= $this->Form->control($screen_shot->id . '.sub_title', ['label' => false, 'value' => $screen_shot->sub_title]) ?>
                    <?= $this->Form->control($screen_shot->id . '.image', ['label' => false, 'type' => 'file']) ?>
                    <label for="thumbnail">
                        <?= $this->Html->image((!empty($screen_shot->image) ? $screen_shot->image : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-' . $screen_shot->id . '-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <?= $this->Form->button(__('Update')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>