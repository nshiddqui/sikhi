<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Events</h3>
        <?= $this->Html->link('Add Event', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-warning pull-right']) ?>&nbsp;
        <?= $this->Html->link('Upload Event', ['action' => 'upload'], ['escape' => false, 'class' => 'btn btn-warning pull-right', 'style' => 'margin-right: 5px;']) ?>
        <div class="col-md-3 pull-right">
            <?= $this->Form->control('date', ['datepicker' => true, 'label' => false, 'placeholder' => 'Enter Date', 'autocomplete' => 'off']) ?>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Events') ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#date').on('change', function () {
            $('#dtEvents').DataTable().draw();
        })
    });
</script>