<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        $this->Html->image((!empty($result->image) ? $result->image : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle']),
        h($result->name),
        h($result->start_date),
        h($result->end_date),
        h($result->search_keywords),
        (!empty($result->link) ? $this->Html->link('click here', $result->link, ['target' => '_BLANK']) : ''),
        h($result->created),
        $this->Html->link($this->Html->image('edit.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'edit', $result->id], ['escape' => false]) .
            $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->title), 'escape' => false])
    ]);
}
echo $this->DataTables->response();
