<?= $this->html->css('event', ['block' => true]) ?>
<?= $this->html->script('event', ['block' => true]) ?>
<?= $this->Form->create($event, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add/Edit Event') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image('not-found.png', ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For Event</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('image', ['type' => 'file', 'class' => 'hidden', 'label' => false]);
                    echo $this->Form->control('name');
                    echo $this->Form->control('link', ['type' => 'website']);
                    echo $this->Form->control('description');
                    ?>
                </div>
                <div class="col-md-12">
                    <label>Is Single Day Event?</label>
                    <?php
                    echo $this->Form->control('is_event', ['type' => 'radio', 'label' => false, 'options' => ['No', 'Yes'],'default' => 0]);
                    echo $this->Form->control('start_date', ['datepicker' => true, 'type' => 'text']);
                    echo $this->Form->control('end_date', ['datepicker' => true, 'type' => 'text']);
                    echo $this->Form->control('search_keywords');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>