<?= $this->html->css('event', ['block' => true]) ?>
<?= $this->html->script('event', ['block' => true]) ?>
<?= $this->Form->create($event, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Upload Events') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Upload ICS File</label>
                    <div class="preview-zone hidden">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <div><b>Preview</b></div>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-danger btn-xs remove-preview">
                                        <i class="fa fa-times"></i> Reset This Form
                                    </button>
                                </div>
                            </div>
                            <div class="box-body"></div>
                        </div>
                    </div>
                    <div class="dropzone-wrapper">
                        <div class="dropzone-desc">
                            <i class="glyphicon glyphicon-download-alt"></i>
                            <p id="file-path">Choose an ICS file or drag it here.</p>
                        </div>
                        <?php
                        echo $this->Form->control('file', ['type' => 'file', 'class' => 'dropzone', 'label' => false, 'accept' => 'text/calendar']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Form->button(__('Upload')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>