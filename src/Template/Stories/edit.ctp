<?php
$this->assign('title', 'Story Management');
?>
<?= $this->html->css('story', ['block' => true]) ?>
<?= $this->html->script('story', ['block' => true]) ?>
<?= $this->html->script('https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js', ['block' => true]) ?>
<?= $this->Form->create($story, ['type' => 'file']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Edit Story') ?></h3>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="image">
                        <?= $this->Html->image((!empty($story['image']) ? $story['image'] : 'not-found.png'), ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For Story</p>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('name', ['label' => 'Story Name']) ?>
                    <?= $this->Form->control('keywords', ['label' => 'Keywords']) ?>
                    <?= $this->Form->control('meta_data', ['label' => 'Meta Data']) ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('age_group', ['label' => 'Select Age Group', 'options' => $age_group]) ?>
                </div>
                <div class="col-md-12">
                    <label>Story Type</label>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->radio('type', $type, ['default' => 0, 'class' => 'type-radio', 'label' => ['class' => 'type-label']]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 type-div" style="display: none;" id="div-type-0">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="plain-text">English</h5>
                            <?= $this->Form->control('plain_english', ['label' => false, 'rows' => 8]) ?>
                            <div class="audio-input">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="audio-plain-english" class="btn btn-primary btn-sm">Choose Audio</label>
                                        <span id="name-audio-plain-english"></span>
                                    </div>
                                    <?php if (!empty($story['audio_plain_english'])) { ?>
                                        <div class="col-md-2 text-right">
                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deleteFile', 'audio_plain_english', $story['id']], ['escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $story['audio_plain_english'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5 class="plain-text">Punjabi</h5>
                            <?= $this->Form->control('plain_punjabi', ['label' => false, 'rows' => 8]) ?>
                            <div class="audio-input">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="audio-plain-punjabi" class="btn btn-primary btn-sm">Choose Audio</label>
                                        <span id="name-audio-plain-punjabi"></span>
                                    </div>
                                    <?php if (!empty($story['audio_plain_punjabi'])) { ?>
                                        <div class="col-md-2 text-right">
                                            <?= $this->Form->postLink('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deleteFile', 'audio_plain_punjabi', $story['id']], ['escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $story['audio_plain_punjabi'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 type-div" style="display: none;" id="div-type-1">
                    <h5 class="plain-text">Upload Content</h5>
                    <table class="w-100">
                        <thead class="hidden">
                            <tr id="defualt-item" class="border-box">
                                <td class="sr-no">::1.</td>
                                <td>
                                    <i class="fa fa-close fa-2x delete-pdf"></i>
                                    <div class="hidden">
                                        <?= $this->Form->hidden('pdf[id][]', ['value' => '']) ?>
                                        <?= $this->Form->control('pdf[english][]', ['label' => false, 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                        <?= $this->Form->control('pdf[punjabi][]', ['label' => false, 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                        <?= $this->Form->control('pdf[audio_english][]', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
                                        <?= $this->Form->control('pdf[audio_punjabi][]', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 border-right">
                                            <label for="pdf-english" class="btn btn-primary btn-sm pdf-btn">Choose English PDF</label>
                                            <span id="name-pdf-english"></span>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="pdf-punjabi" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi PDF</label>
                                            <span id="name-pdf-punjabi"></span>
                                        </div>
                                        <div class="col-md-6 border-right">
                                            <label for="pdf-audio-english" class="btn btn-primary btn-sm pdf-btn">Choose English Audio</label>
                                            <span id="name-pdf-audio-english"></span>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="pdf-audio-punjabi" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi Audio</label>
                                            <span id="name-pdf-audio-punjabi"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody id="append-table">
                            <?php if (!empty($story['pdfs'])) { ?>
                                <?php foreach ($story['pdfs'] as $key => $pdf) { ?>
                                    <?php $key++; ?>
                                    <tr class="border-box">
                                        <td class="sr-no">::<?= $key ?>.</td>
                                        <td>
                                            <?= $this->Html->link('<i class="fa fa-close fa-2x delete-pdf"></i>', ['action' => 'deletePdfFile', $pdf['id']], ['escape' => false]) ?>
                                            <div class="hidden">
                                                <?= $this->Form->hidden('pdf[id][]', ['value' => $pdf['id']]) ?>
                                                <?= $this->Form->control('pdf[english][]', ['label' => false, 'id' => 'pdf-english' . $key, 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                                <?= $this->Form->control('pdf[punjabi][]', ['label' => false, 'id' => 'pdf-punjabi' . $key, 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                                <?= $this->Form->control('pdf[audio_english][]', ['label' => false, 'id' => 'pdf-audio-english' . $key, 'type' => 'file', 'accept' => 'audio/*']) ?>
                                                <?= $this->Form->control('pdf[audio_punjabi][]', ['label' => false, 'id' => 'pdf-audio-punjabi' . $key, 'type' => 'file', 'accept' => 'audio/*']) ?>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 border-right">
                                                    <label for="pdf-english<?= $key ?>" class="btn btn-primary btn-sm pdf-btn">Choose English PDF</label>
                                                    <span id="name-pdf-english<?= $key ?>"></span>
                                                    <?php if (!empty($pdf['english'])) { ?>
                                                        <div class="pull-right mbt-10">
                                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deletePdfFile', 'english', $pdf['id']], ['escape' => false]) ?>
                                                            <?= $this->Html->link('<i class="fa fa-eye fa-2x"></i>', $pdf['english'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="pdf-punjabi<?= $key ?>" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi PDF</label>
                                                    <span id="name-pdf-punjabi<?= $key ?>"></span>
                                                    <?php if (!empty($pdf['punjabi'])) { ?>
                                                        <div class="pull-right mr-30 mbt-10">
                                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deletePdfFile', 'punjabi', $pdf['id']], ['escape' => false]) ?>
                                                            <?= $this->Html->link('<i class="fa fa-eye fa-2x"></i>', $pdf['punjabi'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-6 border-right">
                                                    <label for="pdf-audio-english<?= $key ?>" class="btn btn-primary btn-sm pdf-btn">Choose English Audio</label>
                                                    <span id="name-pdf-audio-english<?= $key ?>"></span>
                                                    <?php if (!empty($pdf['audio_english'])) { ?>
                                                        <div class="pull-right mbt-10">
                                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deletePdfFile', 'audio_english', $pdf['id']], ['escape' => false]) ?>
                                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $pdf['audio_english'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="pdf-audio-punjabi<?= $key ?>" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi Audio</label>
                                                    <span id="name-pdf-audio-punjabi<?= $key ?>"></span>
                                                    <?php if (!empty($pdf['audio_punjabi'])) { ?>
                                                        <div class="pull-right mr-30 mbt-10">
                                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deletePdfFile', 'audio_punjabi', $pdf['id']], ['escape' => false]) ?>
                                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $pdf['audio_punjabi'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr class="border-box">
                                    <td class="sr-no">::1.</td>
                                    <td>
                                        <div class="hidden">
                                            <?= $this->Form->control('pdf[english][]', ['label' => false, 'id' => 'pdf-english1', 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                            <?= $this->Form->control('pdf[punjabi][]', ['label' => false, 'id' => 'pdf-punjabi1', 'type' => 'file', 'accept' => 'application/pdf']) ?>
                                            <?= $this->Form->control('pdf[audio_english][]', ['label' => false, 'id' => 'pdf-audio-english1', 'type' => 'file', 'accept' => 'audio/*']) ?>
                                            <?= $this->Form->control('pdf[audio_punjabi][]', ['label' => false, 'id' => 'pdf-audio-punjabi1', 'type' => 'file', 'accept' => 'audio/*']) ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="pdf-english1" class="btn btn-primary btn-sm pdf-btn">Choose English PDF</label>
                                                <span id="name-pdf-english1"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="pdf-punjabi1" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi PDF</label>
                                                <span id="name-pdf-punjabi1"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="pdf-audio-english1" class="btn btn-primary btn-sm pdf-btn">Choose English Audio</label>
                                                <span id="name-pdf-audio-english1"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="pdf-audio-punjabi1" class="btn btn-primary btn-sm pdf-btn">Choose Punjabi Audio</label>
                                                <span id="name-pdf-audio-punjabi1"></span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" class="plus-item text-center">
                                    <i class="fa fa-plus-circle fa-2x"></i>
                                    <p class="thumbnail-paragraph">Add Content</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                <div class="col-md-12 type-div" style="display: none;" id="div-type-2">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 border-black">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="video-english" class="btn btn-primary btn-sm">Choose English Video</label>
                                        <?= $this->Form->control('video_english', ['label' => false, 'name' => 'video_english_link', 'id' => 'video_english_link', 'type' => 'website', 'placeholder' => 'English Video Link']) ?>
                                        <span id="name-video-english"></span>
                                    </div>
                                    <?php if (!empty($story['video_english'])) { ?>
                                        <div class="col-md-4 pull-right text-right">
                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deleteFile', 'video_english', $story['id']], ['escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $story['video_english'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6 border-black">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="video-punjabi" class="btn btn-primary btn-sm">Choose Punjabi Video</label>
                                        <?= $this->Form->control('video_punjabi', ['label' => false, 'name' => 'video_punjabi_link', 'id' => 'video_punjabi_link', 'type' => 'website', 'placeholder' => 'Punjabi Video Link']) ?>
                                        <span id="name-video-punjabi"></span>
                                    </div>
                                    <?php if (!empty($story['video_punjabi'])) { ?>
                                        <div class="col-md-4 pull-right text-right">
                                            <?= $this->Html->link('<i class="fa fa-trash-o fa-2x"></i>', ['action' => 'deleteFile', 'video_punjabi', $story['id']], ['escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-play-circle fa-2x"></i>', $story['video_punjabi'], ['fancybox' => true, 'data-type' => 'iframe', 'escape' => false]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
    <div class="hidden">
    <?= $this->Form->control('audio_plain_english', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
    <?= $this->Form->control('audio_plain_punjabi', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
    <?= $this->Form->control('video_english', ['label' => false, 'type' => 'file', 'accept' => 'video/*']) ?>
    <?= $this->Form->control('video_punjabi', ['label' => false, 'type' => 'file', 'accept' => 'video/*']) ?>
    <?= $this->Form->control('audio_english', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
    <?= $this->Form->control('audio_punjabi', ['label' => false, 'type' => 'file', 'accept' => 'audio/*']) ?>
    <?= $this->Form->control('image', ['label' => false, 'type' => 'file', 'accept' => 'image/*']) ?>
</div>
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <div class="overlay" id="loader" style="display: none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>
<!-- /.box -->
<?= $this->Form->end() ?>
<script>
        CKEDITOR.replace( 'plain_english' );
        CKEDITOR.replace( 'plain_punjabi' );
</script>