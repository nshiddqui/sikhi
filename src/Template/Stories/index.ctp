<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Stories</h3>
        <?= $this->Html->link('Add Story', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-warning pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Stories') ?>
    </div>
</div>