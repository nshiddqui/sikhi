<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <?= $this->Html->image('user.png?' . microtime(true), ['class' => 'img-circle', 'alt' => 'User Image']) ?>
        </div>
        <div class="pull-left info">
            <p><?= $authUser['name'] ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><?= $this->Html->link('<i class="fa fa-users"></i><span>User Management</span>', ['controller' => 'users'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-book"></i><span>Story Management</span>', ['controller' => 'stories'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-calendar"></i><span>Event Management</span>', ['controller' => 'events'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-file-video-o"></i><span>Media Management</span>', ['controller' => 'medias'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-globe"></i><span>Hukamnama Management</span>', ['controller' => 'huqmnamas'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-bell"></i><span>Notification Management</span>', ['controller' => 'notifications'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-paper-plane"></i><span>Submitted Stories Management</span>', ['controller' => 'SubmittedStories', 'action' => 'index'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-share-square"></i><span>Social Media Management</span>', ['controller' => 'socialMedias', 'action' => 'edit', 1], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-share-square"></i><span>Screenshot Management</span>', ['controller' => 'screenshots', 'action' => 'index'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-user"></i><span>Admin management</span>', ['controller' => 'Admins', 'action' => 'chnageProfile'], ['escape' => false]) ?></li>
    </ul>
</section>