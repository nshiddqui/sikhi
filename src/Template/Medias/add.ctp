<?= $this->html->css('media', ['block' => true]) ?>
<?= $this->html->script('media', ['block' => true]) ?>
<?= $this->Form->create($media, ['type' => 'file', 'id' => 'media']) ?>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= __('Add/Edit Media') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <label for="thumbnail">
                        <?= $this->Html->image('not-found.png', ['class' => 'thumbnail-image', 'id' => 'thumbnail-image', 'default-image' => $this->Url->build(['controller' => 'img', 'action' => 'not-found.png'])]) ?>
                    </label>
                    <p class="thumbnail-paragraph">Thumbnail Image For Event</p>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('thumbnail', ['type' => 'file', 'class' => 'hidden', 'label' => false, 'accept' => 'image/*']);
                    echo $this->Form->control('title');
                    echo $this->Form->control('type', ['options' => $media_type]);
                    echo $this->Form->control('description');
                    echo $this->Form->control('media', ['type' => 'file']);
                    ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->control('link', ['type' => 'website']) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <?= $this->Html->link(__('Cancel'), $this->request->referer(), ['class' => 'btn btn-danger']) ?>
        <?= $this->Form->button(__('Save')) ?>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
<?= $this->Form->end() ?>