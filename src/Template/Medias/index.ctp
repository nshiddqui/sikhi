<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Medias</h3>
        <?= $this->Html->link('Add Media', ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-warning pull-right']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('Medias') ?>
    </div>
</div>