<?php

foreach ($results as $result) {
    $this->DataTables->prepareData([
        h($result->id),
        $this->Html->image((!empty($result->image) ? $imagePath . $result->image : 'not-found.png'), ['style' => 'height: 45px; width: 45px', 'class' => 'img-circle']),
        h($result->title),
        h($result->user_name),
        h($result->user_email),
        h($result->user_phone),
        $age_groups[$result->age_group],
        h($result->created),
        $this->Html->image(strtolower($submitted_story_status[$result->status]) . '.png'),
        $this->Html->link($this->Html->image('view.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'view', $result->id], ['escape' => false]) .
            $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $result->id], ['confirm' => __('Are you sure you want to delete # {0}?', $result->title), 'escape' => false])
    ]);
}
echo $this->DataTables->response();
