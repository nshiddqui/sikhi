<div class="box">
    <div class="box-header">
        <h3 class="box-title">List Submitted Stories</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?= $this->DataTables->render('SubmittedStories') ?>
    </div>
</div>