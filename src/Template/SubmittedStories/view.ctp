<?= $this->Html->css('submitted_story', ['block' => true]) ?>
<?= $this->Html->script('submitted_story', ['block' => true]) ?>
<div class="box-header">
    <h3 class="box-title">Story Details</h3>
    <div class="pull-right status-width">
        <div class="row">
            <div class="col-md-9">
                <?= $this->Form->create(null, ['id' => 'change-status-form', 'url' => ['action' => 'approve', $submittedStory->id]]) ?>
                <?= $this->Form->control('status_id', ['options' => $submitted_story_status, 'default' => $submittedStory->status, 'label' => 'Change Status']) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-3 delete-icon">
                <?= $this->Form->postLink($this->Html->image('delete.png', ['style' => 'height: 20px;margin: 5px;']), ['action' => 'delete', $submittedStory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $submittedStory->id), 'escape' => false, 'title' => 'Delete Story']) ?>
            </div>
        </div>
    </div>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div class="row">
        <div class="col-md-6 text-center">
            <label for="image">
                <?= $this->Html->image((!empty($submittedStory->image) ? $imagePath . $submittedStory->image : 'not-found.png'), ['class' => 'thumbnail-image']) ?>
            </label>
        </div>
        <div class="col-md-6">
            <ul class="nav nav-stacked">
                <li><a href="javascript:void(0)"><strong>Story ID</strong><span class="pull-right"><?= $submittedStory->id ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Title</strong><span class="pull-right"><?= $submittedStory->title ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Full Name</strong><span class="pull-right"><?= $submittedStory->user_name ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Email</strong><span class="pull-right"><?= $submittedStory->user_email ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Phone</strong><span class="pull-right"><?= $submittedStory->user_phone ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Age Group</strong><span class="pull-right"><?= $age_groups[$submittedStory->age_group] ?></span></a></li>
                <li><a href="javascript:void(0)"><strong>Submitted Date &amp; Time</strong><span class="pull-right"><?= $submittedStory->created ?></span></a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header text-center">
                    <h3 class="box-title">Story Details</h3>
                </div>
                <div class="box-body">
                    <pre class="story-details">
                        <?= $this->Text->autoParagraph(h($submittedStory->description)) ?>
                    </pre>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Attachments</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Attachment</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($submittedStory['attachments']) && !empty($submittedStory['attachments'])) { ?>
                                <?php foreach ($submittedStory['attachments'] as $key => $attachment) { ?>
                                    <tr>
                                        <td> <?= $key + 1 ?></td>
                                        <td><?= $attachment->attachment ?></td>
                                        <td><?= $media_types[$attachment->type] ?></td>
                                        <td>
                                            <?= $this->Html->link($this->Html->image('view.png', ['style' => 'height: 20px;margin: 5px;']), $imagePath . $attachment->attachment, ['escape' => false, 'fancybox' => true]) ?>
                                            &nbsp;
                                            <?= $this->Html->link('<i class="fa fa-download"></i>', $imagePath . $attachment->attachment, ['escape' => false, 'download' => true, 'class' => 'text-black']) ?>
                                        </td>
                                    </tr>
                                <?php   } ?>
                            <?php  } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>