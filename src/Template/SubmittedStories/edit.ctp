<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SubmittedStory $submittedStory
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $submittedStory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $submittedStory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Submitted Stories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Attachments'), ['controller' => 'Attachments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Attachment'), ['controller' => 'Attachments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="submittedStories form large-9 medium-8 columns content">
    <?= $this->Form->create($submittedStory) ?>
    <fieldset>
        <legend><?= __('Edit Submitted Story') ?></legend>
        <?php
            echo $this->Form->control('image');
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('email');
            echo $this->Form->control('phone');
            echo $this->Form->control('age_group');
            echo $this->Form->control('details');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
